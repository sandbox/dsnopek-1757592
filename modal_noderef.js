
Drupal.behaviors.modal_noderef = function (context) {
  var defaultWidth = 960, defaultHeight = 400;

  function setupUrl(url) {
    if (url.indexOf('?') >= 0) {
      url += '&';
    }
    else {
      url += '?';
    }
    url += 'automodal=true';

    return url;
  }

  function setupSettings(node) {
    return {
      width: defaultWidth,
      height: defaultHeight,
      url: setupUrl($(node).attr('href')),
    };
  }

  function createHandler() {
    var self = this, field, settings = setupSettings(this);
    
    // setup our URL
    settings.url = $(this).attr('href');
    if (settings.url.indexOf('?') >= 0) {
      settings.url += '&';
    }
    else {
      settings.url += '?';
    }
    settings.url += 'automodal=true';

    // find the input field
    field = $('.modal-noderef-field', $(this).prev());

    // function to stash the nid after the node has been added
    settings.onSubmit = function (data) {
      field.val(data.nid);
      $(self)
        .text(data.title)
        .attr('href', data.href)
        .unbind('click')
        .click(viewHandler);
    };

    Drupal.modalFrame.open(settings);

    return false;
  }

  function viewHandler() {
    var self = this, settings = setupSettings(this);
    settings.onSubmit = function (data) {
      $(self).text(data.title);
    };
    Drupal.modalFrame.open(settings);
    return false;
  }

  $('.modal-noderef-field', context).parent().hide();

  $('.modal-noderef-create-link', context).click(createHandler);
  $('.modal-noderef-view-link', context).click(viewHandler);
};

